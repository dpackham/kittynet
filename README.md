# kittynet

Python libraries (and a simple script) to monitor cats, chiefly through their litterbox usage on a box prepped with load sensors  and a Raspberry Pi.

## Quick start

To get a quick and dirty start with the kittynet project, you're going to need the following pieces of hardware.

* A Raspberry Pi or comparable computer-on-a-chip. For Bluetooth range-sensing you'll definitely want a chip with Bluetooth; for Google or Twilio integration you're definitely going to want network connectivity. I used Raspberry Pi Zero W boards, which are inexpensive, small, and more than capable enough for this light duty. Future iterations of this project might outgrow the Zero's processor, but at present it's more than sufficient.
* An HX711 and load sensors; inexpensive kits of a single HX711 and four 50kg load sensors are commonly available on Amazon and elsewhere.
* A means to attach the load sensors to your litterbox. 
* (optional) Collar-mounted Bluetooth devices for each cat. I used Mi Band 2s and custom-crocheted sheaths to hold each onto the kitty's collar.
* (optional) If you're putting this on a Catgenie 120 which has a DRM-breaking Arduino, an appropriate USB cable to communicate between the Arduino and the Raspberry Pi.

Wire up the HX711 and load sensors according to a standard Wheatstone bridge design. There's tons of tutorials out there on this, and chances are your HX711 kit has wiring instructions. It's worth looking at those instructions because, while standard 3-wire load sensors have red, white, and black wires, manufacturers are a bit inconsistent on which are which (on my kit the red wires went to the HX711 and the white and black formed the bridge, but yours may vary).

Connect the HX711 to your Raspberry Pi, with the VCC and GND pins connected to 3.3V and ground pins respectively. The data (DT or DOUT) and clock (SCK) pins should be connected to two GPIO pins; record their BCM pin numbeers.

If using a Catgenie Arduino CR14 emulator, you're going to slightly modify its code at this time to report its doings over USB. Before line 55 (the return from receiveEvent() with usage-count reports) add the line "Serial.println(remaining);" and at the beginning of setup(),before line 142, add "Serial.begin(9600);". Blow this slightly improved code onto your Arduino, and connect the Arduino over USB to the Raspberry Pi.

If you're using Google Sheets, GMail, or Twilio, you need to get the appropriate API credentials. Twilio credentials are put in the config file; Google credentials will be put in a JSON file (and will need an OAuth verification on the first execution).

Push the library and panopticon.py as well as the three directories static, templates, and panopticon.d onto your Pi, and run pip3 -r requirements.txt to get all the necessary libraries.

Edit the two configuration files in panopticon.d. If you're going to set up multiple litterboxes, they'll all have a different local configuration, but can use the same global configuration. Most of the settings should be moderately self-evident, because thereare detailed comments in the configuration files. You can comment out most services not being used: so for instance commenting out the entire Catgenie, Fake-Catgenie, and every LED and Switch section is appropriate; you might also choose to comment out GMail, GSheets, and/or Twilio, depending what services you're using. If you're not Bluetooth-tagging your cats, you can comment out every Cat section too. Make sure to put the right pin numbers into the HX711 sections. Set the HX711 ratio to 1 for the time being; you'll need to change this as well as possibly the Weight section and/or Fake-Catgenie after you get some calibration data. You almost certainly want to leave the logging level at "info" for your first run.

Run panopticon.py as root (root is optional if you give the right permissions to bluepy-helper, but it's a lot more straightforward). The log should report each service as it comes up. GMail and Google Sheets will give you instructions to authorize the app using a web browser; follow those instructions (ideally, this will only need to be done once).

If all went well, visiting the IP address of your Raspberry Pi on a web browser should bring up the "kittynet dashboard", whose most prominent feature is a line graph showing the current weight on the load sensors. Place a calibrated weight in the litterbox and determine how much the reported weight changes; divide the change in apparent weight by the actual weight to determine the appropriate ratio to put in your HX711 configuration. This may be negative, depending how you wired up the load sensors: if putting a weight in the box makes the reported weight go down, the sensors are wired backwards but this isn't actually a problem. If you're using units other than kilograms, make sure the numbers in the "Weight" section and, if using it, the "Fake-Catgenie" section make sense.

Restart the panopticon script, and bask in the warm glow of a cat-monitoring station! If you later choose to add LEDs, physical switches, or the like, hopefully the configuration files are reasonably straightforward.

## Library-use details

The library provides services by connecting servers (which are sources of data on your cats) to clients (ways of recording or reporting that data), by way of "pipelines" which filter or process the data.

The servers currently supported include:
* GPIO-connected HX711 board (suitable for managing load sensors)
* GPIO-connected digital 1-bit sensor devices (e.g. HC-SR501s or simmilar PIR motion detectors) 
* Bluetoth devices, polled for RSSI
  * Battery level detection on Mi Band 2
* Serial input (particularly for a Catgenie-managing Arduino)

The clients currently supported include:
* GPIO-connected LEDs
* Local plaintext logging
* GMail
* Google Sheets.
* Flask integration for webapp development

Pipelines are nearly infinitely customizable by putting code into a generic pipeline, but particularly relevant prefab pipelines include:
* One-shot battery-level alerts
* GPIO-connected physical switches as message filters
* Aggregators to bundle single numeric reports from statistics over a fixed time or number of reports
* Weight-determination state machines to bundle "notable" weight events
* RSSI-loggers to place relevant RSSI data into a bundled weight event

All of these features are used in the included "panopticon" script, and looking at this script can hopefully illuminate the use of these various abilities.

Still to be done:
* Twilio servers for SMS messages
* More Bluetooth devices
* More intelligent weight-determination states
* More intelligent identification of individual cats
