import datetime
import pigpio
import datetime
import binascii

from .common import Server
from numbers import Number

# All this data comes from David Hamp-Gonsalves's exhaustive I2C
# snooping on communication between a CatGenie and its real CR14 RFID
# reader. More information at
# https://www.davidhampgonsalves.com/reverse-engineering-cat-genie-120-drm/

CATGENIE_I2C_ADDRESS = 0x50
CATGENIE_BLOCKS = [
    bytearray([0x04,0x00,0x00,0x00,0x00]), # Block 0
    bytearray([0x00]),                     # Block 1 (never happens?)
    bytearray([0x00]),                     # Block 2 (never happens?)
    bytearray([0x00]),                     # Block 3 (never happens?)
    bytearray([0x00]),                     # Block 4 (never happens?)
    bytearray([0x04,0x78,0x00,0x00,0x00]), # Block 5
    bytearray([0x04,0x78,0x00,0x00,0x00]), # Block 6
    bytearray([0x00]),                     # Block 7 (never happens?)
    bytearray([0x04,0x01,0x00,0x78,0x00]), # Block 8
    bytearray([0x04,0x01,0x00,0x78,0x00]), # Block 9
    bytearray([0x04,0x01,0x00,0x78,0x00]), # Block A
    bytearray([0x04,0x04,0x0F,0x00,0x00]), # Block B
    bytearray([0x04,0x04,0x0F,0x00,0x00]), # Block C
    bytearray([0x04,0x1F,0x7E,0x30,0x2A]), # Block D
    bytearray([0x00]),                     # Block E (never happens?)
    bytearray([0x04,0x04,0x0F,0x2B,0xFC])  # Block F
]

CATGENIE_READ_SELECT_CMD = bytearray([0x01,0x02,0x06,0x00])
CATGENIE_READ_NODE_ID = bytearray([0x01,0x02,0x0E,0x3C])
CATGENIE_READ_UID = bytearray([0x01,0x01,0x0B])

CATGENIE_NODE_ID_RESPONSE = bytearray([0x01,0x3C])
CATGENIE_UID_RESPONSE = bytearray([0x08,0xA3,0x6E,0x6A,0x73,0x09,0x33,0x02,0xD0])


class SimpleTrigger(Server):
    """Simple trigger interface: reads rising edge from given GPIO pin"""
    def __init__(self,pin,name="Trigger",cooldown=60):
        if(isinstance(cooldown,Number)):
            self.cooldown=datetime.timedelta(seconds=cooldown)
        else:
            self.cooldown=cooldown
        self.lastedge=datetime.datetime.now()-self.cooldown
        self.name=name
        self.pin=pin
        self.event = None
        self.pi=pigpio.pi()
        if not self.pi.connected:
            raise IOError('Failed to instantiate GPIO handler')
        self.start()
        super(SimpleTrigger,self).__init__()

    def edge_detect(self,gpio,level,tick):
        if (datetime.datetime.now()-self.lastedge>self.cooldown):
            self.lastedge=datetime.datetime.now()
            self.broadcast_data({"name":self.name,
                                 "time":datetime.datetime.now()})

    def __del__(self):
        self.stop()

    def stop(self):
        if self.event is not None:
            self.event.cancel()
        
    def start(self):
        if self.event is None:
            self.event = self.pi.callback(self.pin,pigpio.RISING_EDGE,self.edge_detect)


class CatGenie(Server):
    """CatGenie interface: while active, it masquerades as a CR-14 RFID
reader, and triggers a message whenever the cartridge is modified
(i.e. whenever a wash cycle is triggered by the CatGenie
litterbox). CatGenie must be connnected to the appropriate GPIO
pins.
    """
    def __init__(self,name="CatGenie"):
        self.name=name
        self.event = None
        self.pi=pigpio.pi()
        if not self.pi.connected:
            raise IOError('Failed to instantiate GPIO handler')
        self.washcount = 120
        self.start()
        super(CatGenie,self).__init__()
        
    def __del__(self):
        self.stop()

    def i2c_handler(self,data,tick):
        status, length, data = self.pi.bsc_i2c(CATGENIE_I2C_ADDRESS)
        print("I2C: Recieved triple {:b},{},{}".format(status,length,binascii.hexlify(data)))
        if(length<4):
            # All messages of length 1 or 2 can be ignored
            print("I2C mesage ignored.")
            return
        elif(data.find(CATGENIE_READ_SELECT_CMD)>-1):
            print("I2C message identified as READ_SELECT_CMD, sending response.")
            if (status & 0x10)==0:
                print("FIFO has contents; no new contents.")
                return
            self.pi.bsc_i2c(CATGENIE_I2C_ADDRESS,CATGENIE_NODE_ID_RESPONSE)
        elif(data.find(CATGENIE_READ_NODE_ID)>-1):
            print("I2C message identified as READ_NODE_ID, sending response.")
            if (status & 0x10)==0:
                print("FIFO has contents; no new contents.")
                return
            self.pi.bsc_i2c(CATGENIE_I2C_ADDRESS,CATGENIE_NODE_ID_RESPONSE)
        elif(length>3 and data[0:3]==CATGENIE_READ_UID):
            print("I2C message identified as READ_UID, sending response.")
            if (status & 0x10)==0:
                print("FIFO has contents; no new contents.")
                return
            self.pi.bsc_i2c(CATGENIE_I2C_ADDRESS,CATGENIE_UID_RESPONSE)
        else:
            print("UNKNOWN I2C message: {}".format(binascii.hexlify(data)))
#        elif(length==4):
#            print("I2C message identified as request for block {}.".format(data[3]))
            # Request for an internal block; blocks 5 and 6 are special.
#            outbytes=bytearray(CATGENIE_BLOCKS[1])
#            if(data[3]==5 or data[3]==6):
#                outbytes[1] = self.washcount
#            print("I2C response: {}".format(binascii.hexlify(outbytes)))
#            self.pi.bsc_i2c(CATGENIE_I2C_ADDRESS,outbytes)
#        elif(length>4):
#            print("I2C message identified as washcount update to {}.".format(data[4]))
            # Update to wash count --- this means a wash has been triggered!
#            self.washcount = data[4]
#            self.broadcast_data({"name":self.name,
#                                 "wash":self.washcount,
#                                 "time":datetime.datetime.now()})

    def stop(self):
        if self.event is not None:
            self.event.cancel()
            self.pi.bsc_i2c(0)
        
    def start(self):
        if self.event is None:
            self.event = self.pi.event_callback(pigpio.EVENT_BSC, self.i2c_handler)
            self.pi.bsc_i2c(CATGENIE_I2C_ADDRESS)

