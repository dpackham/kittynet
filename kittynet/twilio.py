"""Twilio integration form KittyNet

This package has a simple client which uses the Twilio API to report
messages via SMS."""

import twilio.rest

from .common import Client

class Twilio(Client):
    """A Kittynet client which formats received messages as strings, and
    then sends them to Twilio's API to be sent through the Short
    Message Service (SMS) to a cellphone.

    Parameters
    ----------
    sid : str
        The account SID associated with the Twilio account to be used.
    token : str
        The Twilio authorization token to be used.
    sender : str
        The phone number (in international format, with the "+"
        prefix) of the sending number/shortcode. This is most likely a
        number particular to your Twilio account.
    recipient : str
        The phone number (in international format, with the "+"
        prefix) of the number to which messages should be sent.
    text : str
        Text of the messages sent. This string will be formatted using
        the method :meth:`str.format` and can include placeholders
        expanded into message values.

    """

    def __init__(self,sid,token,sender,recipient,text="{msg}"):
        self._client=twilio.rest.Client(sid,token)
        self.sender=sender
        self.recipient=recipient
        self.text=text
        super().__init__()

    def handle_data(self,message):
        """The message being handled is sent to the Twilio API

        Parameters
        ----------
        message : dict
            Message received from a server, to be sent.
        """
        self._client.messages.create(to=self.recipient,
                                     from_=self.sender,
                                     body=self.text.format(**message))

