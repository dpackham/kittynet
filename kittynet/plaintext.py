"""Plaintext local I/O services for KittyNet.

The primary I/O service is a client which writes to a text
filehandle. This is good both for thin, non-networked recording of
data to a file, or for debugging purposes when sent to STDOUT or
STDERR. Judicious use of log lines can produce a format suitable for
import as a CSV into a spreadsheet.

"""

import datetime,copy

from .common import Client

class DebugLogger(Client):
    """A log-to-file client that recapitulates the full content of
    messages on a regular basis.
    Parameters
    ----------
    filehandle : file
        Text-mode file object to be written to. Most likely either the
        result of an :func:`open` command, or the system filehandles
        :data:`sys.stderr` or :data:`sys.stdout`.

    prefix : str, optional
        A prefix to put before the message, to help debug.

    entries : int, optional
        Used to reduce report spam; if specified, then the logger will
        report every nth message (for this value of n), instead of
        every message.
    """
    def __init__(self,filehandle,prefix="",entries=1):
        self.filehandle=filehandle
        self._prefix=prefix
        self._entries=entries
        self._counter=entries
        super().__init__()
        
    def handle_data(self,message):
        """The message being handled is written to the filehandle.

        Parameters
        ----------
        message : dict
            Message received from a server, to be used to populate
            fields in text to be written to the log.

        """
        self._counter -= 1
        if self._counter==0:
            self._counter=self._entries
            print((self._prefix + "{}").format(message))

class Logger(Client):

    """A simple but configurable log-to-file client.

    Parameters
    ----------
    filehandle : file
        Text-mode file object to be written to. Most likely either the
        result of an :func:`open` command, or the system filehandles
        :data:`sys.stderr` or :data:`sys.stdout`.

    logline : str
        A line of text to write to the log with each message
        received. This string will be formatted using the method
        :meth:`str.format` and can include placeholders expanded into
        message values.

    Attributes
    ---------- 
    filehandle : file
        Text-mode file object to be written to.

    logline : str
        A line of text to write to the log with each message
        received.
    """
    def __init__(self,filehandle,
                 logline="{time},{name},{rssi},{battery}"):
        self.filehandle=filehandle
        self.logline=logline
        super().__init__()
        
    def handle_data(self,message):
        """The appropriate logline produced by the message being handled is
        written to the filehandle.

        Parameters
        ----------
        message : dict
            Message received from a server, to be used to populate
            fields in text to be written to the log.

        """
        self.filehandle.write(self.logline.format(**message) + "\n")
