"""Simple Kittynet-to-Flask integration

This package provides a simple Kittynet client associated with a Flask
instance, which posts all received messages to a configured path. This
can be used for creating live charts online with Chart.js, or any
other web services that integrate well with Flask.

Setup of every Flask parameter except for the specific path in
question is relegated to the user. This allows one to, e.g. build two
separate Kittynet endpoints connected to different paths in a single
Flask instance.
"""


import flask
import json
import threading
import time
import datetime
from numbers import Number

from .common import Server,Client

class FlaskPath(Client):
    """A Kittynet client which passes all received messages along to an
    associated Flask instance. All Flask setup, aside from the specific
    route associated with this object, must be done separately.

    Parameters
    ----------
    app : :class:`flask.Flask`
        The underlying flask instance to which data is posted.
    path : str, optional
        Path within the flask instance for the data endpoint. By
        default it is "/kittynet"
    buffer_size : float, int, or :class:`datetime.timedelta`, optional
        Length of the internal buffer used to post data to
        clients. The default of 20 seconds should be good; increase if
        you see data being "dropped". Making this _very_ large could
        allow an ongoing client to "pre-populate" charts which have
        just been opened.

    """

    def __init__(self,app,path="/kittynet",buffer_length=20):
        self._app=app
        self._buffer=[]
        self._bufferlock=threading.Lock()
        self._bufferchanged=threading.Event()
        self._tareID=0
        self._taremessage=None
        self._headers=[]
        if(isinstance(buffer_length,Number)):
            self._buffer_length=datetime.timedelta(seconds=buffer_length)
        else:
            self._buffer_length=buffer_length
        self._nextID=0
        self._app.add_url_rule(path,endpoint=str(self),view_func=self._data_responder)
        super().__init__()

    def add_header_message(self,message):
        self._headers.append(message)
        
    def _data_loop(self):
        msgID=-1
        for msg in self._headers:
            json_data = json.dumps(msg,default=str)
            yield f"data:{json_data}\n\n"
        while True:
            self._bufferchanged.wait()
            self._bufferlock.acquire()
            # Tare messages are special
            if self._taremessage is not None and self._tareID>msgID:
                json_data = json.dumps(self._taremessage,default=str)
                yield f"data:{json_data}\n\n"
            for msg in self._buffer:
                if msgID is None or msgID<msg["ID"]:
                    msgID=msg["ID"]
                    json_data = json.dumps(msg["data"],default=str)
                    yield f"data:{json_data}\n\n"
            self._bufferlock.release()
            self._bufferchanged.clear()

    def _data_responder(self):
        r=flask.Response(self._data_loop(), mimetype='text/event-stream')
        r.call_on_close(self._bufferlock.release)
        return r

    def handle_data(self,message):
        """The message being handled is dropped into the message buffer, and
        old messages are purged.

        Parameters
        ----------
        message : dict
            Message received from a server, to be used to populate the buffer.
        """
        self._bufferlock.acquire()
        if "wtype" in message and message["wtype"]=="tare":
            # Tare messages are special; only keep the most recent!
            self._taremessage=message
            self._tareID=self._nextID
        else:
            self._buffer.append({"ID":self._nextID,"data":message})
            while self._buffer[0]["data"]["time"]+self._buffer_length<message["time"]:
                self._buffer.pop(0)
        self._nextID += 1
        self._bufferlock.release()
        self._bufferchanged.set()
